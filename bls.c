/*
bls

Linux file list program designed for very large directories.

Copyright 2018, Simon Allen

Adapted from the getdents64() definition on the Linux Programmer's Manual.
*/

#define _GNU_SOURCE

#include <dirent.h>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/syscall.h>

struct linux_dirent64 {
	ino64_t			d_ino;
	off64_t			d_off;
	unsigned short	d_reclen;
	unsigned char	d_type;
	char			d_name[];
};

#define BUF_SIZE 1024 * 1024 * 5

int main(int argc, char *argv[])
{
	int fd = 0;
	int nread = 0;
	int pos = 0;
	char buf[BUF_SIZE] = {'\0'};
	struct linux_dirent64 *d = NULL;

	fd = open(argc > 1 ? argv[1] : ".", O_RDONLY | O_DIRECTORY);

	if (fd == -1)
		error_at_line(EXIT_FAILURE, errno, __FILE__, __LINE__, "open()");

	for (;;) {
		nread = syscall(SYS_getdents64, fd, buf, BUF_SIZE);

		if (nread == -1)
			error_at_line(EXIT_FAILURE, errno, __FILE__, __LINE__, "syscall()");

		if (nread == 0)
			break;

		for (pos = 0; pos < nread;) {
			d = (struct linux_dirent64 *)(buf + pos);

			if (d->d_ino == 0)
				continue;

			printf("%-18s\t%s\n",
			       (d->d_type == DT_REG) ?  "regular file" :
			       (d->d_type == DT_DIR) ?  "directory" :
			       (d->d_type == DT_FIFO) ? "named pipe (FIFO)" :
			       (d->d_type == DT_SOCK) ? "UNIX domain socket" :
			       (d->d_type == DT_LNK) ?  "symlink link" :
			       (d->d_type == DT_BLK) ?  "block device" :
			       (d->d_type == DT_CHR) ?  "character device" : "???",
			       d->d_name);

			pos += d->d_reclen;
		}
	}

	return EXIT_SUCCESS;
}
